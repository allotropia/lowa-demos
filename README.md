# lowa-demos

[TOC]

## Getting started

Set up a LOWA installation. See:  
https://git.libreoffice.org/core/+/refs/heads/master/static/README.wasm.md

After building LOWA add this before the `</body>` of your `instdir/program/qt_soffice.html` to load a demo.

```html
    <script type="text/javascript" src="bindings_uno.js"></script>
    <script type="text/javascript" src="DEMO_FILE.js"></script>
```

## JavaScript Console Debugging

Some hints to debug the demos from the JavaScript console of your browser.

### C++ Type Information For JS Variables

You may like to know C++ type information for a JS variable from the demos. Lets say for `xModel`.

Get the C++ interface a variable is bound to.  
`JS_VARIABLE.$$.ptrType.name`  
You may look up the interface documentation on https://api.libreoffice.org/docs/idl/ref/

&nbsp;

Note: api.libreoffice.org has only documentation about interfaces, not about classes.
```js
# Get the actual C++ class of the object.
service_info = css.lang.XServiceInfo.query(JS_VARIABLE);
service_info.getImplementationName();

# Get list of service names for an object.
service_names = service_info.getSupportedServiceNames();
for (let i=0; i<service_names.size(); i++) {
    console.log(service_names.get(i))
}
```

&nbsp;

Get the list of interfaces implemented by the object.
```js
type_provider = css.lang.XTypeProvider.query(JS_VARIABLE);
for (let i=0; i<type_provider.getTypes().size(); i++) {
    console.log(type_provider.getTypes().get(i).toString())
}
```

&nbsp;

Get the properties ob an object.
```js
property_set = css.beans.XPropertySet.query(JS_VARIABLE);
props = property_set.getPropertySetInfo().getProperties();
for (let i=0; i<props.size(); i++) {
    val = 'ERROR: RuntimeError';
    try {
        val = property_set.getPropertyValue(props.get(i).Name).get()
    } catch (RuntimeError) {}
    console.log(props.get(i).Name + ': ' + val)
}
```

## Roadmap
1. Build perfect solution.
2. Go on vacation ;-)
