// SPDX-License-Identifier: MPL-2.0
// Based on demo from libreoffice.git/static/README.wasm.md by Sarper Akdemir and Stephan Bergmann.
"use strict";

// Make variables accessible from the console for debugging.
let css, xModel, xTextDocument, xText, xTextCursor, xTextRange;

console.log('PLUS: poll and wait for Embind "Module"');  // not needed for QT5
const interval = setInterval(function() {
    if (typeof Module === 'undefined') return;
    clearInterval(interval);
    console.log('PLUS: wait a fixed amount of seconds for LO UI and UNO to settle');
    setTimeout(function() {
        console.log('PLUS: execute example code');

        // Inserts a string at the start of the Writer document.
        css = init_unoembind_uno(Module).com.sun.star;
        xModel = Module.getCurrentModelFromViewSh();
        xTextDocument = css.text.XTextDocument.query(xModel);
        xText = xTextDocument.getText();
        xTextCursor = xText.createTextCursor();
        xTextCursor.setString("string here!");

        // Disabled, so variables can be accessed from console.
        // Embind-3.1.56 does this if the variables are locally scoped, but
        // warns it's not reliable.
        // Seems not actually to delete the object if there are other references
        // to it. Probably some smart pointer logic.
        //[xTextCursor, xText, xTextDocument, xModel].forEach((o) => o.delete());
    }, 10000);
}, 10);
