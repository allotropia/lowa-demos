// SPDX-License-Identifier: MPL-2.0
// Based on demo from libreoffice.git/static/README.wasm.md by Sarper Akdemir and Stephan Bergmann.
"use strict";

// Make variables accessible from the console for debugging.
let css, xModel, xTextDocument, xText, xEnumAccess, xParaEnumeration;

console.log('PLUS: poll and wait for Embind "Module"');  // not needed for QT5
const interval = setInterval(function() {
    if (typeof Module === 'undefined') return;
    clearInterval(interval);
    console.log('PLUS: wait a fixed amount of seconds for LO UI and UNO to settle');
    setTimeout(function() {
        console.log('PLUS: execute example code');

        // Changes each paragraph of the Writer document to a random color.
        css = init_unoembind_uno(Module).com.sun.star;
        xModel = Module.getCurrentModelFromViewSh();
        xTextDocument = css.text.XTextDocument.query(xModel);
        xText = xTextDocument.getText();
        xEnumAccess = css.container.XEnumerationAccess.query(xText);
        xParaEnumeration = xEnumAccess.createEnumeration();

        while (xParaEnumeration.hasMoreElements()) {
            const xParaProps = css.beans.XPropertySet.query(xParaEnumeration.nextElement().get());
            const uno_long = Module.uno_Type.Long();
            const color = new Module.uno_Any(uno_long, Math.floor(Math.random() * 0xFFFFFF));
            uno_long.delete();  // Embind-3.1.56 can do this, but warns it's not reliable.
            xParaProps.setPropertyValue("CharColor", color);
            xParaProps.delete();  // Embind-3.1.56 can do this, but warns it's not reliable.
            // No auto delete for color. Because Embind sees it as a pointer, not a smart pointer.
            color.delete();
        }

        // Disabled, so variables can be accessed from console.
        // Embind-3.1.56 does this if the variables are locally scoped, but
        // warns it's not reliable.
        // Seems not actually to delete the object if there are other references
        // to it. Probably some smart pointer logic.
        //[xParaEnumeration, xEnumAccess, xText, xTextDocument, xModel].forEach((o) => o.delete());
    }, 10000);
}, 10);
