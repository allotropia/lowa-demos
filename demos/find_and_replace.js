// SPDX-License-Identifier: MPL-2.0
"use strict";

// Make variables accessible from the console for debugging.
let css, xModel, xSearchable, searchDescriptor, xInterface, xTextCursor;

console.log('PLUS: poll and wait for Embind "Module"');  // not needed for QT5
const interval = setInterval(function() {
    console.log('looping');
    if (typeof Module === 'undefined') return;
    clearInterval(interval);
    console.log('PLUS: wait 10 seconds for LO UI and UNO to settle');
    setTimeout(function() {  // Waits 10 seconds for UNO.
        console.log('PLUS: execute example code');

        // Replaces every occurence of "LibreOffice" with "LIBRE-OFFICE YEAH".
        css = init_unoembind_uno(Module).com.sun.star;
        xModel = Module.getCurrentModelFromViewSh();

        xSearchable = css.util.XSearchable.query(xModel);
        searchDescriptor = xSearchable.createSearchDescriptor();
        searchDescriptor.setSearchString('LibreOffice');

        xInterface = xSearchable.findFirst(searchDescriptor);
        while (xInterface !== null) {
            const xTextCursor = css.text.XTextCursor.query(xInterface);
            xInterface.delete();  // Embind-3.1.56 can do this, but warns it's not reliable.
            //xTextCursor.goRight(0, false);  // Appending instead of replacing.
            xTextCursor.setString("LIBRE-OFFICE YEAH");
            xInterface = xSearchable.findNext(xTextCursor, searchDescriptor);
            xTextCursor.delete();
        }

        // Disabled, so variables can be accessed from console.
        // Embind-3.1.56 does this if the variables are locally scoped, but
        // warns it's not reliable.
        // Seems not actually to delete the object if there are other references
        // to it. Probably some smart pointer logic.
        //[xTextCursor, xInterface, searchDescriptor, xSearchable, xModel].forEach((o) => o.delete());
    }, 10000);
}, 0.1);
